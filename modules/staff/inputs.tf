variable "domain" {
  default = "gmail.com"
  type    = string
}

variable "given_name" {
  type = string
}

variable "family_name" {
  type = string
}
