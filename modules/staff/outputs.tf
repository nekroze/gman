output "initial_password" {
  value = module.gsuite.initial_password
}

output "email_address" {
  value = module.gsuite.email_address
}
