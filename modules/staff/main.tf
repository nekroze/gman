module "gsuite" {
  source = "../platforms/gsuite"

  domain      = var.domain
  given_name  = var.given_name
  family_name = var.family_name
}

module "slack" {
  source = "../platforms/slack"

  team_workspace_id = 98765 # TODO: change to your company's team/workspace id
  email_address     = module.gsuite.email_address
  channels = [
    12345 # TODO: set to channel id number for your companies "general" or "welcome" channel
  ]
}
