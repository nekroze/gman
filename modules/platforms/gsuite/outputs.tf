output "initial_password" {
  value = gsuite_user.main.password
}

output "email_address" {
  value = gsuite_user.main.primary_email
}
