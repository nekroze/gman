resource "gsuite_user" "main" {
  name {
    family_name = var.family_name
    given_name  = var.given_name
  }

  primary_email = "${lower(var.given_name)}.${lower(var.family_name)}@${var.domain}"

  # If omitted or `true` existing GSuite users defined as Terraform resources will be imported by `terraform apply`.
  update_existing = true
}
