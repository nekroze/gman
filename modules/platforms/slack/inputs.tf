variable "email_address" {
  type = string
}

variable "team_workspace_id" {
  type = number
}

variable "channels" {
  type = list(number)
}
