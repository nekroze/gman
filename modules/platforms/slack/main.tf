# Expects https://github.com/Mastercard/terraform-provider-restapi

locals {
  body = { # as described in https://api.slack.com/methods/admin.users.invite
    "token" : "${data.aws_caller_identity.current.account_id}",
    "channel_ids" = var.channels,
    "email"       = var.email_address,
    "team_id"     = var.team_workspace_id,
  }
}

resource "null_resource" "invite" {
  provisioner "local-exec" {
    command = <<EOT
        curl --fail --silent \
             -X POST https://slack.com/api/admin.users.invite \
             -H 'Content-Type: application/json' \
             -H 'Authorization: Bearer ${var.auth_token}' \
             -d '${jsonencode(local.body)}'
EOT
  }
}
