module "steven_mctowley" {
  source = "./modules/staff"

  given_name   = "Steven"
  familty_name = "McTowley"
  domain       = "example.com"
}

output "steven_mctowleys_initial_password" {
  value = module.steven_mctowley.initial_password
}
