# gman

Corporate User Orchestration through terraform POC

## Explanation

The root of this repo has a `staff.tf` file that defines each user, simply copy paste the staff module and change the names to add another user.

Each user in `staff.tf` invokes the `modules/staff` module which invokes both the `modules/platforms/gsuite` and `modules/platforms/slack` modules.

The `modules/platforms/gsuite` module will create a user complete with email address in a corporate gsuite account. A password is randomly generated and must be replaced the first time the user logs in. This password is displayed at the the end of a `terraform apply` as an output.

The `modules/platforms/slack` module will send an invite to a slack workspace/team (defined in `modules/staff/main.tf`) to the newly created gsuite email address and add them to a starter channel.
